﻿Imports System.ComponentModel
Public Class Form1
    Private Mt As Form1Metier = Nothing
    Private DbdingS As New BindingSource
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Mt = New Form1Metier(My.Application.Info.DirectoryPath & "\BDD\TEST.accdb")
        Me.DataGridView1.DataSource = DbdingS
        DbdingS.DataSource = Mt.LireTable1()
    End Sub
    Private Sub Form1_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Mt.CloseConnexion()
    End Sub
    Private Sub EnregistrerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EnregistrerToolStripMenuItem.Click
        Mt.MajTable1(DbdingS.DataSource)
    End Sub
    Private Sub QuiterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuiterToolStripMenuItem.Click
        Me.Close()
    End Sub
End Class
