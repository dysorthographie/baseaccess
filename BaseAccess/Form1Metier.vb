﻿Imports ConnexionRd

Public Class Form1Metier
    Inherits Connexion
    Public Sub New(ByVal Fichier As String)
        MyBase.New(Fichier) 'ascenseur
    End Sub



    Public Function LireTable1() As DataTable
        Return DataReaderToDataTable(Execute("Select * from Table1"), "Table1")
    End Function
    Sub MajTable1(ByRef MyTable As DataTable)
        Dim lstRemoved As Integer() = MyTable.Rows.Cast(Of DataRow)().Where(Function(g) g.RowState = DataRowState.Deleted).Select(Function(t) Integer.Parse(t("Id", DataRowVersion.Original).ToString())).ToArray()
        If (lstRemoved.Count() > 0) Then
            Execute($"DELETE FROM {MyTable.TableName} WHERE [ID] in ({String.Join(",", lstRemoved)})")
        End If


        For Each d As DataRow In MyTable.Rows.Cast(Of DataRow)().Where(Function(g) g.RowState = DataRowState.Modified OrElse g.RowState = DataRowState.Added)
            Select Case d.RowState
                Case DataRowState.Modified
                    UpdateTable1(d("ID").ToString, d("NOM").ToString, d("Prénom").ToString, MyTable.TableName)
                Case DataRowState.Added
                    InsertTable1(d("NOM").ToString, d("Prénom").ToString, MyTable.TableName)
            End Select
            'If d.RowState <> DataRowState.Added Then d.AcceptChanges() Else d.SetAdded(True)

            d.AcceptChanges()

        Next
        MyTable = LireTable1()
    End Sub
    Private Sub UpdateTable1(ID As String, Nom As String, Prénom As String, Table As String)
        Dim prm() As ParamCmd = {New ParamCmd, New ParamCmd, New ParamCmd}
        With prm(0)
            .Name = "@Nom" : .Size = 255 : .Type = ConstType.adWChar : .Value = Nom
        End With
        With prm(1)
            .Name = "@Prénom" : .Size = 255 : .Type = ConstType.adWChar : .Value = Prénom
        End With
        With prm(2)
            .Name = "@ID" : .Size = 16 : .Type = ConstType.adBigInt : .Value = ID
        End With
        Execute($"UPDATE [{Table}] SET [Nom] =?,[Prénom]=? WHERE [Id]=?", prm)
    End Sub
    Sub InsertTable1(Nom As String, Prénom As String, Table As String)
        Dim prm() As ParamCmd = {New ParamCmd, New ParamCmd}
        With prm(0)
            .Name = "@Nom" : .Size = 255 : .Type = ConstType.adWChar : .Value = Nom
        End With
        With prm(1)
            .Name = "@Prénom" : .Size = 255 : .Type = ConstType.adWChar : .Value = Prénom
        End With
        Execute($"Insert into [{Table}] ([Nom],[Prénom]) Values(?,?)", prm)
    End Sub
End Class
