﻿Imports System.Data.OleDb

Public Class Connexion
    Public Structure ParamCmd
        Public Name As String
        Public Type As ConstType
        Public Direction As ConstDirection
        Public Size As Long
        Public Value As String
    End Structure

    Public Enum ConstDirection
        adParamUnknown = 0      ' Indique que la direction du paramètre est inconnue.
        adParamInput = 1        ' Par défaut. Indique que le paramètre représente un paramètre d'entrée.
        adParamOutput = 2       ' Indique que le paramètre représente un paramètre de sortie.
        adParamInputOutput = 3   ' Indique que le paramètre représente à la fois un paramètre d’entrée et de sortie.
        adParamReturnValue = 4  ' Indique que le paramètre représente une valeur de retour.
    End Enum

    Public Enum ConstType
        adEmpty = 0             ' Indicates an eight-byte signed integer (DBTYPE_I8).
        adSmallInt = 2          ' Indicates a binary value (DBTYPE_BYTES).
        adInteger = 3           ' Indicates a Boolean value (DBTYPE_BOOL).
        adSingle = 4            ' Indicates a null-terminated character string (Unicode) (DBTYPE_BSTR).
        adDouble = 5            ' Indicates a four-byte chapter value that identifies rows in a child rowset (DBTYPE_HCHAPTER).
        adCurrency = 6          ' Indicates a string value (DBTYPE_STR).
        adDate = 7              ' Indicates a currency value (DBTYPE_CY). Currency is a fixed-point number with four digits to the right of the decimal point. It is stored in an eight-byte signed integer scaled by 10,000.
        adBSTR = 8              ' Indicates a date value (DBTYPE_DATE).
        adIDispatch = 9         ' Indicates a date value (yyyymmdd) (DBTYPE_DBDATE).
        adError = 10            ' Indicates a time value (hhmmss) (DBTYPE_DBTIME).
        adBoolean = 11          ' Indicates a date/time stamp (yyyymmddhhmmss plus a fraction in billionths) (DBTYPE_DBTIMESTAMP).
        adVariant = 12          ' Indicates an exact numeric value with a fixed precision and scale (DBTYPE_DECIMAL).
        adIUnknown = 13         ' Indicates a double-precision floating-point value (DBTYPE_R8).
        adDecimal = 14          ' Specifies no value (DBTYPE_EMPTY).
        adTinyInt = 16          ' Indicates a 32-bit error code (DBTYPE_ERROR).
        adUnsignedTinyInt = 17  ' Indicates a 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601 (DBTYPE_FILETIME).
        adUnsignedSmallInt = 18 ' Indicates a globally unique identifier (GUID) (DBTYPE_GUID).
        adUnsignedInt = 19      ' Indicates a pointer to an IDispatch interface on a COM object (DBTYPE_IDISPATCH).
        adBigInt = 20           ' Indicates a four-byte signed integer (DBTYPE_I4).
        adUnsignedBigInt = 21   ' Indicates a pointer to an IUnknown interface on a COM object (DBTYPE_IUNKNOWN).
        adFileTime = 64         ' Indicates a long binary value.
        adGUID = 72             ' Indicates a long string value.
        adBinary = 128          ' Indicates a long null-terminated Unicode string value.
        adChar = 129            ' Indicates an exact numeric value with a fixed precision and scale (DBTYPE_NUMERIC).
        adWChar = 130           ' Indicates an Automation PROPVARIANT (DBTYPE_PROP_VARIANT).
        adNumeric = 131         ' Indicates a single-precision floating-point value (DBTYPE_R4).
        adUserDefined = 132     ' Indicates a two-byte signed integer (DBTYPE_I2).
        adDBDate = 133          ' Indicates a one-byte signed integer (DBTYPE_I1).
        adDBTime = 134          ' Indicates an eight-byte unsigned integer (DBTYPE_UI8).
        adDBTimeStamp = 135     ' Indicates a four-byte unsigned integer (DBTYPE_UI4).
        adChapter = 136         ' Indicates a two-byte unsigned integer (DBTYPE_UI2).
        adPropVariant = 138     ' Indicates a one-byte unsigned integer (DBTYPE_UI1).
        adVarNumeric = 139      ' Indicates a user-defined variable (DBTYPE_UDT).
        adVarChar = 200         ' Indicates a binary value.
        adLongVarChar = 201     ' Indicates a string value.
        adVarWChar = 202        ' Indicates an Automation Variant (DBTYPE_VARIANT).
        adLongVarWChar = 203    ' Indicates a numeric value.
        adVarBinary = 204       ' Indicates a null-terminated Unicode character string.
        adLongVarBinary = 205   ' Indicates a null-terminated Unicode character string (DBTYPE_WSTR).
    End Enum

    Public Enum CommAdo
        dCmdUnspecified = -1    ' Does not specify the command type argument.
        adCmdText = 1           ' Evaluates CommandText as a textual definition of a command or stored procedure call.
        adCmdTable = 2          ' Evaluates CommandText as a table name whose columns are all returned by an internally generated SQL query.
        adCmdStoredProc = 4     ' Evaluates CommandText as a stored procedure name.
        adCmdUnknown = 8        ' Default. Indicates that the type of command in the CommandText property is not known.
        adCmdFile = 256         ' Evaluates CommandText as the file name of a persistently stored Recordset. Used with Recordset.Open or Requery only.
        adCmdTableDirect = 512  ' Evaluates CommandText as a table name whose columns are all returned. Used with Recordset.Open or Requery only. 
    End Enum

    Private cnx As OleDbConnection = Nothing  ' Correction IDE0044 : Marking cnx as ReadOnly
    Private _Fichier As String  ' Correction IDE0044 : Marking _Fichier as ReadOnly

    Public Sub New(ByVal Fichier As String)
        _Fichier = Fichier
    End Sub
    Public Sub New()

    End Sub
    Public Property Base As String
        Get
            Return _Fichier
        End Get
        Set(value As String)
            _Fichier = value
        End Set
    End Property

    Protected Function OpenConnexion()
        Dim GenereCSTRING As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & _Fichier & ";"
        cnx = New OleDbConnection(GenereCSTRING)
        Try
            cnx.Open()
            Return True
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Protected Function Execute(ByVal Sql As String) As OleDbDataReader
        If cnx Is Nothing Then OpenConnexion()
        Using cmd As New OleDbCommand(Sql, cnx)  ' Correction IDE0140 : Simplification de la création d'objets
            Try
                Return cmd.ExecuteReader()
            Catch ex As Exception
                Return Nothing
            End Try
        End Using
    End Function

    Protected Function Execute(ByVal Sql As String, ByVal Param As Object) As OleDbDataReader
        If cnx Is Nothing Then OpenConnexion()
        Using cmd As New OleDbCommand(Sql, cnx)  ' Correction IDE0140 : Simplification de la création d'objets
            For Each c As Object In Param
                If c.Size <> 0 Then cmd.Parameters.Add(c.Name, c.Type, c.Size).Value = c.Value
            Next
            Try
                Return cmd.ExecuteReader()
            Catch ex As Exception
                Return Nothing
            End Try
        End Using
    End Function

    Protected Function DataReaderToDataTable(DR As OleDbDataReader) As DataTable
        Using TD As New DataTable
            TD.Load(DR)
            Return TD
        End Using
    End Function

    Protected Function DataReaderToDataTable(DR As OleDbDataReader, TableName As String) As DataTable
        Using TD As New DataTable
            TD.Load(DR)
            TD.TableName = TableName
            Return TD
        End Using
    End Function

    Public Sub CloseConnexion()
        cnx.Close()
        cnx.Dispose()
        cnx = Nothing
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
